-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: mysql1.cs.clemson.edu
-- Generation Time: Apr 29, 2016 at 10:03 PM
-- Server version: 5.5.49-0ubuntu0.12.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cpsc399LoginApp`
--

-- --------------------------------------------------------

--
-- Table structure for table `Answers`
--

CREATE TABLE IF NOT EXISTS `Answers` (
  `idAnswers` int(11) NOT NULL AUTO_INCREMENT,
  `idCourse` int(11) DEFAULT NULL,
  `idStudent` int(11) DEFAULT NULL,
  `dateAsked` date DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `answer` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idAnswers`),
  KEY `idStudent` (`idStudent`),
  KEY `idCourse` (`idCourse`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `Answers`
--

INSERT INTO `Answers` (`idAnswers`, `idCourse`, `idStudent`, `dateAsked`, `question`, `answer`) VALUES
(1, 1, 2, '2016-04-14', NULL, '3'),
(2, 1, 15, '2016-04-27', NULL, '0'),
(3, 1, 15, '2016-04-27', NULL, '0'),
(4, 1, 15, '2016-04-27', NULL, '0'),
(5, 1, 15, '2016-04-27', NULL, '0'),
(6, 1, 15, '2016-04-27', NULL, '0'),
(7, 1, 15, '2016-04-27', NULL, '0'),
(8, 1, 15, '2016-04-27', NULL, '0'),
(9, 1, 15, '2016-04-27', NULL, '0'),
(10, 1, 15, '2016-04-27', NULL, '0'),
(11, 1, 15, '2016-04-27', NULL, '0'),
(12, 1, 15, '2016-04-27', NULL, '0'),
(13, 1, 15, '2016-04-27', NULL, '0'),
(14, 1, 15, '2016-04-27', NULL, '0'),
(15, 1, 15, '2016-04-27', NULL, '0'),
(16, 1, 15, '2016-04-27', NULL, '0'),
(17, 1, 15, '2016-04-27', NULL, '0'),
(18, 1, 15, '2016-04-27', NULL, '0'),
(19, 1, 15, '2016-04-27', NULL, '0'),
(20, 1, 30, '2016-04-27', NULL, '0'),
(21, 1, 30, '2016-04-27', NULL, '0'),
(22, 1, 30, '2016-04-27', NULL, '0'),
(23, 1, 30, '2016-04-27', NULL, 'D'),
(24, 1, 30, '2016-04-27', NULL, 'B'),
(25, 1, 33, '2016-04-29', NULL, 'a'),
(26, 1, 33, '2016-04-29', NULL, 'B'),
(27, 1, 33, '2016-04-29', NULL, 'C'),
(28, 1, 33, '2016-04-29', NULL, 'D'),
(29, 1, 33, '2016-04-29', NULL, 'E');

-- --------------------------------------------------------

--
-- Table structure for table `Attendance`
--

CREATE TABLE IF NOT EXISTS `Attendance` (
  `idAttendance` int(11) NOT NULL AUTO_INCREMENT,
  `idCourse` int(11) DEFAULT NULL,
  `idStudent` int(11) DEFAULT NULL,
  `classDate` date DEFAULT NULL,
  `present` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idAttendance`),
  KEY `idStudent` (`idStudent`),
  KEY `idCourse` (`idCourse`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Attendance`
--

INSERT INTO `Attendance` (`idAttendance`, `idCourse`, `idStudent`, `classDate`, `present`) VALUES
(1, 1, 1, '0000-00-00', NULL),
(2, 1, 1, '2016-04-14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Beacon`
--

CREATE TABLE IF NOT EXISTS `Beacon` (
  `idBeacon` int(11) NOT NULL AUTO_INCREMENT,
  `UUID` varchar(45) DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `minor` int(11) DEFAULT NULL,
  `macAddr` varchar(45) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idBeacon`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `Beacon`
--

INSERT INTO `Beacon` (`idBeacon`, `UUID`, `major`, `minor`, `macAddr`, `name`) VALUES
(1, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', 35445, 37670, 'E0:C5:93:26:8A:75', 'mint green'),
(2, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', 26819, 33810, 'F3:0A:84:12:68:C3', 'icy marshmallow'),
(3, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', 65035, 24542, 'F0:B1:5F:DE:FE:0B', 'blueberry pie');

-- --------------------------------------------------------

--
-- Table structure for table `Course`
--

CREATE TABLE IF NOT EXISTS `Course` (
  `idCourse` int(11) NOT NULL AUTO_INCREMENT,
  `idStudent` int(11) DEFAULT NULL,
  `idProfessor` int(11) DEFAULT NULL,
  `idBeacon` int(11) DEFAULT NULL,
  `className` varchar(45) DEFAULT NULL,
  `dayOfWeek` varchar(45) DEFAULT NULL,
  `startTime` time DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  PRIMARY KEY (`idCourse`),
  KEY `idStudent` (`idStudent`),
  KEY `idProfessor` (`idProfessor`),
  KEY `idBeacon` (`idBeacon`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `Course`
--

INSERT INTO `Course` (`idCourse`, `idStudent`, `idProfessor`, `idBeacon`, `className`, `dayOfWeek`, `startTime`, `endTime`) VALUES
(1, 2, 1, 1, 'testClass', 'MWF', '12:00:00', '12:50:00'),
(2, 1, 1, 1, 'test', NULL, NULL, NULL),
(20, 1, 1, 1, 'test', '', '01:00:00', '01:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `courseQuestion`
--

CREATE TABLE IF NOT EXISTS `courseQuestion` (
  `idCourseQuestion` int(11) NOT NULL AUTO_INCREMENT,
  `idCourse` int(11) DEFAULT NULL,
  `idQuestion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCourseQuestion`),
  KEY `idCourse_idx` (`idCourse`),
  KEY `idQuestion_idx` (`idQuestion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `courseQuestion`
--

INSERT INTO `courseQuestion` (`idCourseQuestion`, `idCourse`, `idQuestion`) VALUES
(2, 1, 1),
(3, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Professor`
--

CREATE TABLE IF NOT EXISTS `Professor` (
  `idProfessor` int(11) NOT NULL AUTO_INCREMENT,
  `idStudent` int(11) DEFAULT NULL,
  `emailAddress` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idProfessor`),
  KEY `idStudent_idx` (`idStudent`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Professor`
--

INSERT INTO `Professor` (`idProfessor`, `idStudent`, `emailAddress`) VALUES
(1, 1, 'test@email.com');

-- --------------------------------------------------------

--
-- Table structure for table `Question`
--

CREATE TABLE IF NOT EXISTS `Question` (
  `idQuestion` int(11) NOT NULL AUTO_INCREMENT,
  `questionText` varchar(255) DEFAULT NULL,
  `optionA` varchar(45) DEFAULT NULL,
  `optionB` varchar(45) DEFAULT NULL,
  `optionC` varchar(45) DEFAULT NULL,
  `optionD` varchar(45) DEFAULT NULL,
  `optionE` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idQuestion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Question`
--

INSERT INTO `Question` (`idQuestion`, `questionText`, `optionA`, `optionB`, `optionC`, `optionD`, `optionE`) VALUES
(1, 'this is a test question', NULL, NULL, NULL, NULL, NULL),
(2, 'this is a test question', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Student`
--

CREATE TABLE IF NOT EXISTS `Student` (
  `idStudent` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  PRIMARY KEY (`idStudent`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `Student`
--

INSERT INTO `Student` (`idStudent`, `name`, `email`, `username`, `password`, `dateCreated`) VALUES
(1, 'jesus', 'jmarque@g.clemson.edu', 'jmarque', '1234', NULL),
(2, NULL, NULL, 'jesus', NULL, NULL),
(3, NULL, NULL, 'jesus', NULL, NULL),
(4, NULL, NULL, 'jesus3', NULL, NULL),
(5, NULL, NULL, 'hey  ', NULL, NULL),
(6, NULL, NULL, 'test34', NULL, NULL),
(9, NULL, NULL, 'testuser', NULL, NULL),
(10, NULL, NULL, 'testuser', NULL, NULL),
(11, NULL, NULL, 'tgj', NULL, NULL),
(12, NULL, NULL, 'poo', NULL, NULL),
(13, NULL, NULL, 'tot', NULL, NULL),
(14, NULL, NULL, 'tot', NULL, NULL),
(15, NULL, NULL, 'acrimin', NULL, NULL),
(16, NULL, NULL, 'acrimin', NULL, NULL),
(17, NULL, NULL, 'acrimin', NULL, NULL),
(18, NULL, NULL, 'Acrimin', NULL, NULL),
(19, NULL, NULL, 'hello', NULL, NULL),
(20, NULL, NULL, 'pregs', NULL, NULL),
(21, NULL, NULL, 'Acrimin', NULL, NULL),
(22, NULL, NULL, 'hello', NULL, NULL),
(23, NULL, NULL, 'Acrimin', NULL, NULL),
(24, NULL, NULL, 'Acrimin', NULL, NULL),
(25, NULL, NULL, 'yay', NULL, NULL),
(26, NULL, NULL, 'jmarque', NULL, NULL),
(27, NULL, NULL, 'Acrimin', NULL, NULL),
(28, NULL, NULL, 'test', NULL, NULL),
(29, NULL, NULL, 'time test', NULL, '2016-04-27 00:23:41'),
(30, NULL, NULL, 'post tre', NULL, '2016-04-27 00:41:29'),
(31, NULL, NULL, 'emulate', NULL, '2016-04-27 14:42:23'),
(32, NULL, NULL, 'dcriminski', NULL, '2016-04-29 12:11:26'),
(33, NULL, NULL, 'creigh3', NULL, '2016-04-29 12:11:57'),
(34, NULL, NULL, 'creigh3', NULL, '2016-04-29 12:14:59'),
(50, 'test', 'hello@email.com', 'hello', '1234', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Answers`
--
ALTER TABLE `Answers`
  ADD CONSTRAINT `Answers_ibfk_1` FOREIGN KEY (`idStudent`) REFERENCES `Student` (`idStudent`),
  ADD CONSTRAINT `Answers_ibfk_2` FOREIGN KEY (`idCourse`) REFERENCES `Course` (`idCourse`);

--
-- Constraints for table `Attendance`
--
ALTER TABLE `Attendance`
  ADD CONSTRAINT `Attendance_ibfk_1` FOREIGN KEY (`idStudent`) REFERENCES `Student` (`idStudent`),
  ADD CONSTRAINT `Attendance_ibfk_2` FOREIGN KEY (`idCourse`) REFERENCES `Course` (`idCourse`);

--
-- Constraints for table `Course`
--
ALTER TABLE `Course`
  ADD CONSTRAINT `Course_ibfk_1` FOREIGN KEY (`idStudent`) REFERENCES `Student` (`idStudent`),
  ADD CONSTRAINT `Course_ibfk_2` FOREIGN KEY (`idProfessor`) REFERENCES `Professor` (`idProfessor`),
  ADD CONSTRAINT `Course_ibfk_3` FOREIGN KEY (`idBeacon`) REFERENCES `Beacon` (`idBeacon`);

--
-- Constraints for table `courseQuestion`
--
ALTER TABLE `courseQuestion`
  ADD CONSTRAINT `idCourse` FOREIGN KEY (`idCourse`) REFERENCES `Course` (`idCourse`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idQuestion` FOREIGN KEY (`idQuestion`) REFERENCES `Question` (`idQuestion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Professor`
--
ALTER TABLE `Professor`
  ADD CONSTRAINT `idStudent` FOREIGN KEY (`idStudent`) REFERENCES `Student` (`idStudent`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
