package com.example.airport;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;

import java.util.List;
import java.util.UUID;

public class MainActivity extends Activity {
    private static final int icy = 26819;
    private static final int mint = 35445;
    private static final int blueberry = 65035;
    Toolbar toolbar;


    //private static final Map<String, List<String>> PLACES_BY_BEACONS;
    //
    //static {
    //    Map<String, List<String>> placesByBeacons = new HashMap<>();
    //    placesByBeacons.put("22504:48827", new ArrayList<String>() {{
    //        add("Heavenly Sandwiches");
    //        // read as: "Heavenly Sandwiches" is closest
    //        // to the beacon with major 22504 and minor 48827
    //        add("Green & Green Salads");
    //        // "Green & Green Salads" is the next closest
    //        add("Mini Panini");
    //        // "Mini Panini" is the furthest away
    //    }});
    //    placesByBeacons.put("648:12", new ArrayList<String>() {{
    //        add("Mini Panini");
    //        add("Green & Green Salads");
    //        add("Heavenly Sandwiches");
    //    }});
    //    PLACES_BY_BEACONS = Collections.unmodifiableMap(placesByBeacons);
    //}
    //
    //private List<String> placesNearBeacon(Beacon beacon) {
    //    String beaconKey = String.format("%d:%d", beacon.getMajor(), beacon.getMinor());
    //    if (PLACES_BY_BEACONS.containsKey(beaconKey)) {
    //        return PLACES_BY_BEACONS.get(beaconKey);
    //    }
    //    return Collections.emptyList();
    //}

    private BeaconManager beaconManager;
    private Region region;
    private TextView textView;
    private LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        layout = (LinearLayout) findViewById(R.id.layout);

        beaconManager = new BeaconManager(this);
        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
             @Override
             public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                 if (!list.isEmpty()) {
                     Log.e("list size: ", list.size() + "");
                     for (Beacon b : list) {
                         if (b.getMajor() == icy)
                             addRow(b);
                         else if (b.getMajor() == mint)
                             addRow(b);
                         else if (b.getMajor() == blueberry)
                             addRow(b);
                     }
                 }
             }

         }

        );

        region = new Region("ranged region", UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), null, null);
    }

    private void addRow(Beacon b) {
        LinearLayout row = new LinearLayout(this);

        TextView name = new TextView(this);
        TextView major = new TextView(this);
        TextView minor = new TextView(this);
        TextView power = new TextView(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ActionBar.LayoutParams.WRAP_CONTENT, 1f);
        params.setMargins(5, 5, 5, 5);

        name.setLayoutParams(params);
        major.setLayoutParams(params);
        minor.setLayoutParams(params);
        power.setLayoutParams(params);

        switch (b.getMajor()) {
            case icy:
                name.setText("Icy Blue");

                break;
            case mint:
                name.setText("Mint Green");
                break;
            case blueberry:
                name.setText("Blueberry");
                break;
        }
        major.setText(b.getMajor() + "");
        minor.setText(b.getMinor() + "");
        power.setText(b.getRssi() + "");


        row.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout.LayoutParams llparam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llparam.setMargins(5, 5, 5, 5);

        row.setLayoutParams(llparam);
        row.addView(name);
        row.addView(major);
        row.addView(minor);
        row.addView(power);
        layout.addView(row, 0);

    }

    @Override
    protected void onResume() {
        super.onResume();
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });
    }

    @Override
    protected void onPause() {
        beaconManager.stopRanging(region);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private Boolean stopped = false;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if (stopped) {
                stopped = false;
                beaconManager.startRanging(region);
            } else {
                stopped = true;
                beaconManager.stopRanging(region);
            }
            return true;
        } else if (id == R.id.clear) {
            layout.removeAllViews();
        }

        return super.onOptionsItemSelected(item);
    }
}
